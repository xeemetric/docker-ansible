FROM ubuntu:16.04

MAINTAINER Dmitry Korobitsin <korobicin@gmail.com>

RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu xenial main" | tee /etc/apt/sources.list.d/ansible.list && \
    echo "deb-src http://ppa.launchpad.net/ansible/ansible/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/ansible.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 7BB9C367 && \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y ansible && \
    apt-get install -y sshpass openssh-client && \
    echo 'localhost' > /etc/ansible/hosts

CMD [ "/bin/bash" ]
